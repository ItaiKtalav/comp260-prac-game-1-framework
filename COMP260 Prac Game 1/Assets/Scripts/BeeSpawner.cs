﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawner : MonoBehaviour {

    public Bee beePrefab;
    public int beeNumInit;
    public float spawnRadius;

    public float minSpawnTime;
    public float maxSpawnTime;

    private float timeToSpawn;

    // Use this for initialization
    void Start()
    {

        // create bees
        for (int i = 0; i < beeNumInit; i++)
        {
            createRandomBee();
        }

        timeToSpawn = Time.time + UnityEngine.Random.Range(minSpawnTime, maxSpawnTime);

    }

    void createRandomBee()
    {
        // instantiate a bee
        Bee bee = Instantiate(beePrefab);
        Vector2 pos = transform.position;
        pos.x += UnityEngine.Random.Range(-spawnRadius, spawnRadius);
        bee.transform.position = pos;
    }

    // Update is called once per frame
    void Update () {

        if (timeToSpawn < Time.time)
        {
            createRandomBee();
            timeToSpawn = Time.time + UnityEngine.Random.Range(minSpawnTime, maxSpawnTime);
        } 

	}
}
