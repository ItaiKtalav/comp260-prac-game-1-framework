﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bee : MonoBehaviour
{

    public float speed;        // metres per second
    public float turnSpeed;  // degrees per second
    private Transform target; //this is the one it's currently heading too
    public Vector2 heading = Vector3.right;
    public ParticleSystem explosionPrefab;
	private float angle;

    void start()
    {
        Player p = FindObjectOfType<Player>();
        target = p.transform;

        // bee initially moves in random direction
        heading = Vector2.right;
        angle = Random.value * 360;
        heading = heading.Rotate(angle);

        // set speed and turnSpeed randomly 
        speed = Mathf.Lerp(5, 15, Random.value);
        turnSpeed = Mathf.Lerp(90, 200, Random.value);
    }

    void Update()
    {

        Player[] players = FindObjectsOfType(typeof(Player)) as Player[];
        // find which one is closer
        foreach (Player p in players){
            if (target == null)
            {
                target = p.transform;
            }
            else
            {
                if (Vector2.Distance(transform.position, target.position) > Vector2.Distance(transform.position, p.transform.position))
                {
                    target = p.transform;
                }
            }
        }

        // get the vector from the bee to the target 
        Vector2 direction = target.position - transform.position;

        // calculate how much to turn per frame
        float angle = turnSpeed * Time.deltaTime;

        // turn left or right
        if (direction.IsOnLeft(heading))
        {
            // target on left, rotate anticlockwise
            heading = heading.Rotate(angle);
        }
        else {
            // target on right, rotate clockwise
            heading = heading.Rotate(-angle);
        }

        transform.Translate(heading * speed * Time.deltaTime);
    }

    void OnDrawGizmos()
    {
        // draw heading vector in red
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, heading);

        // draw target vector in yellow
        Gizmos.color = Color.yellow;
        if (target != null)
        {
            Vector2 direction = target.position - transform.position;
            Gizmos.DrawRay(transform.position, direction);
        }
        
    }


    public void OnDestroy()
    {
        kill();
    }

    public void kill()
    {
        Destroy(gameObject);
        // create an explosion at the bee's current position
        ParticleSystem explosion = Instantiate(explosionPrefab);
        explosion.transform.position = transform.position;
    }
}
